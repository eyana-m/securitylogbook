class Office < ActiveRecord::Base
	has_many :log_records

	def to_s
		"#{name}"
	end
end
