class LogRecordsController < ApplicationController
  #before_action :set_log_record, only: [:show, :edit, :update, :destroy]

  # GET /log_records
  def index
    @log_records = LogRecord.all
  end

  # GET /log_records/1
  def show
    @log_record = LogRecord.find(params[:id])
  end

  # GET /log_records/new
  def new
    @log_record = LogRecord.new
  

  end

  # GET /log_records/1/edit
  def edit
     @log_record = LogRecord.find(params[:id])
  end

  # POST /log_records
  def create
    @log_record = LogRecord.new(log_record_params)
    @log_record.user = current_user
   
      if @log_record.save
        redirect_to @log_record, notice: 'Log record was successfully created.' 
        
      else
        render action: 'new'         
      end
    
  end

  # PATCH/PUT /log_records/1
  def update
     @log_record = LogRecord.find(params[:id])
      if @log_record.update(log_record_params)
        redirect_to @log_record, notice: 'Log record was successfully updated.' 
        
      else
        render action: 'edit' 
  
      end
   
  end

  # DELETE /log_records/1
  def destroy
    @log_record = LogRecord.find(params[:id])
    @log_record.destroy
     redirect_to log_records_url   

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log_record
      @log_record = LogRecord.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def log_record_params
      params.require(:log_record).permit!
    end
end
