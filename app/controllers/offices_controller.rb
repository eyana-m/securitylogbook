class OfficesController < ApplicationController
  #before_action :set_office, only: [:show, :edit, :update, :destroy]

  # GET /offices
  def index
    @offices = Office.all
  end

  # GET /offices/1
  def show
    @office = Office.find(params[:id])
  end

  # GET /offices/new
  def new
    @office = Office.new
  end

  # GET /offices/1/edit
  def edit
    @office = Office.find(params[:id])
  end

  # POST /offices
  def create
    @office = Office.new(office_params)

      if @office.save
       redirect_to @office, notice: 'Office was successfully created.' 
       
      else
        render action: 'new'
        
      end
   
  end

  # PATCH/PUT /offices/1
  def update
    @office = Office.find(params[:id])
      if @office.update(office_params)
        redirect_to @office, notice: 'Office was successfully updated.'
      
      else
        render action: 'edit' 
      end
  end

  # DELETE /offices/1
  def destroy
    @office = Office.find(params[:id])
    @office.destroy
    redirect_to offices_path 
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office
      @office = Office.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_params
      params.require(:office).permit!
    end
end
