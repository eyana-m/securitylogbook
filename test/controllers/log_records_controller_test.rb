require 'test_helper'

class LogRecordsControllerTest < ActionController::TestCase
  setup do
    @log_record = log_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:log_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create log_record" do
    assert_difference('LogRecord.count') do
      post :create, log_record: { office_id: @log_record.office_id, purpose_of_visit: @log_record.purpose_of_visit, time_of_visit: @log_record.time_of_visit, user_id: @log_record.user_id, visitor_name: @log_record.visitor_name }
    end

    assert_redirected_to log_record_path(assigns(:log_record))
  end

  test "should show log_record" do
    get :show, id: @log_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @log_record
    assert_response :success
  end

  test "should update log_record" do
    patch :update, id: @log_record, log_record: { office_id: @log_record.office_id, purpose_of_visit: @log_record.purpose_of_visit, time_of_visit: @log_record.time_of_visit, user_id: @log_record.user_id, visitor_name: @log_record.visitor_name }
    assert_redirected_to log_record_path(assigns(:log_record))
  end

  test "should destroy log_record" do
    assert_difference('LogRecord.count', -1) do
      delete :destroy, id: @log_record
    end

    assert_redirected_to log_records_path
  end
end
